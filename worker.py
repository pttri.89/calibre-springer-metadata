#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__   = 'GPL v3'
__copyright__ = '2018, Tri Phan <pttri89@gmail.com>'
__docformat__ = 'restructuredtext en'

import socket, re, datetime, urlparse
from collections import OrderedDict
from threading import Thread

from lxml.html import fromstring, tostring

from calibre.ebooks.metadata.book.base import Metadata
from calibre.library.comments import sanitize_comments_html
from calibre.utils.cleantext import clean_ascii_chars

import calibre_plugins.springer_metadata.config as cfg

class Worker(Thread): # Get details

    '''
    Get book details from Springer book page in a separate thread
    '''

    def __init__(self, url, result_queue, browser, log, relevance, plugin, timeout=20):
        Thread.__init__(self)
        self.daemon = True
        self.url, self.result_queue = url, result_queue
        self.log, self.timeout = log, timeout
        self.relevance, self.plugin = relevance, plugin
        self.browser = browser.clone_browser()
        self.cover_url = self.isbn_id = self.isbn = None

    def run(self):
        try:
            self.get_details()
        except:
            self.log.exception('get_details failed for url: %r'%self.url)

    def get_details(self):
        try:
            self.log.info('Springer url: %r'%self.url)
            raw = self.browser.open_novisit(self.url, timeout=self.timeout).read().strip()
        except Exception as e:
            if callable(getattr(e, 'getcode', None)) and \
                    e.getcode() == 404:
                self.log.error('URL malformed: %r'%self.url)
                return
            attr = getattr(e, 'args', [None])
            attr = attr if attr else [None]
            if isinstance(attr[0], socket.timeout):
                msg = 'Springer timed out. Try again later.'
                self.log.error(msg)
            else:
                msg = 'Failed to make details query: %r'%self.url
                self.log.exception(msg)
            return

        raw = raw.decode('utf-8', errors='replace')
        #open('F:\\barnesnoble.html', 'wb').write(raw)

        if '<title>404 - ' in raw:
            self.log.error('URL malformed: %r'%self.url)
            return

        try:
            root = fromstring(clean_ascii_chars(raw))
        except:
            msg = 'Failed to parse Springer details page: %r'%self.url
            self.log.exception(msg)
            return

        self.parse_details(root)

    def parse_details(self, root):
        for e in root.iter("span"):
            if "display:none" in e.get("style", "").replace(" ", ""):
               e.text = ""
                
        try:
            springer_isbn_id = self.parse_springer_isbn_id(self.url)
            self.log.info('Springer ISBN is: %s' % springer_isbn_id)
        except:
            self.log.exception('Error parsing Springer ISBN id for url: %r'%self.url)
            springer_isbn_id = None

        try:
            (title, series, series_index) = self.parse_title_series(root)
        except:
            self.log.exception('Error parsing title and series for url: %r'%self.url)
            title = series = series_index = None

        try:
            authors = self.parse_authors(root)
        except:
            self.log.exception('Error parsing authors for url: %r'%self.url)
            authors = []

        if not title or not authors or not springer_isbn_id:
            self.log.error('Could not find title/authors/Springer ISBN id for %r'%self.url)
            self.log.error('Springer ISBN id: %r Title: %r Authors: %r'%(springer_isbn_id, title,
                authors))
            return

        mi = Metadata(title, authors)
        self.log.info('parse_details - mi:', mi)
        if series:
            mi.series = series
            mi.series_index = series_index
        mi.set_identifier('springer_isbn', springer_isbn_id)
        self.springer_isbn_id = springer_isbn_id

        try:
            isbn = self.parse_isbn(root)
            if isbn:
                self.isbn = mi.isbn = isbn
        except:
            self.log.exception('Error parsing ISBN for url: %r'%self.url)

        try:
            mi.rating = self.parse_rating(root)
        except:
            self.log.exception('Error parsing ratings for url: %r'%self.url)

        try:
            mi.comments = self.parse_comments(root)
        except:
            self.log.exception('Error parsing comments for url: %r' % self.url)

        try:
            self.cover_url = self.parse_cover(root)
        except:
            self.log.exception('Error parsing cover for url: %r' % self.url)
        mi.has_cover = bool(self.cover_url)

        try:
            tags = self.parse_tags(root)
            if tags:
                mi.tags = tags
        except:
            self.log.exception('Error parsing tags for url: %r' % self.url)

        mi.cover_url = self.cover_url # This is purely so we can run a test for it!!!

        try:
            mi.publisher = self.parse_publisher(root)
        except:
            self.log.exception('Error parsing publisher for url: %r' % self.url)

        try:
            mi.pubdate = self.parse_published_date(root)
        except:
            self.log.exception('Error parsing published date for url: %r' % self.url)


        try:
            lang = self._parse_language(root)
            if lang:
                mi.language = lang
        except:
            self.log.exception('Error parsing language for url: %r'%self.url)

        mi.source_relevance = self.relevance

        if self.springer_isbn_id:
            if self.isbn:
                self.plugin.cache_isbn_to_identifier(self.isbn, self.springer_isbn_id)
            if self.cover_url:
                self.plugin.cache_identifier_to_cover_url(self.springer_isbn_id,
                        self.cover_url)

        self.plugin.clean_downloaded_metadata(mi)
        self.result_queue.put(mi)

    def parse_springer_isbn_id(self, url):
        return re.search('springer.com/gp/book/(\d+)', url).groups(0)[0]

    def parse_title_series(self, root):
        title_node = root.xpath('//dd[contains(@itemprop, "name")]')
        series_node = root.xpath('//dd/a[contains(@href, "series")]')
        series_index_node = root.xpath('//dd[contains(@itemprop, "publisher")]/preceding-sibling::dd[position()=2]')

        if not title_node:
            self.log('Aborting search for title')
            return (None, None, None)
        title_text = title_node[0].text_content().strip()
        #self.log('Found title text:',title_text)
        # if title_text.endswith('/'):
        #     title_text = title_text[:-1].strip()

        if not series_node:
            #self.log('Title has no parenthesis for series so done:',title_text)
            return (title_text, None, None)
        
        series_name = series_node[0].text_content().strip()
        series_index = series_index_node[0].text_content().strip()
        #self.log('Title has series info as follows:',title, 'Series:',series_name, 'Idx:',series_index)
        return (title_text, series_name, series_index)


    def parse_authors(self, root):
        author_nodes = root.xpath('//li[contains(@itemprop, "author")]/span')

        # if authors is empty, we find the editors
        if not author_nodes:
            author_nodes = root.xpath('//li[contains(@itemprop, "editor")]/span')

        if author_nodes:
            # self.log.info('Found %d authors' % len(author_nodes))
            authors = []
            for author_node in author_nodes:
                author = tostring(author_node, method='text', encoding=unicode).strip()
                if author:
                    authors.append(author)
            return authors

    def parse_rating(self, root):
       return None

    def parse_isbn(self, root):
        detail_nodes = root.xpath('//dd[contains(@itemprop, "isbn")]')[0]
        if detail_nodes is not None:
            return detail_nodes.text_content().strip().replace('-', '')

    def parse_publisher(self, root):
        detail_nodes = root.xpath('//dd[contains(@itemprop, "publisher")]/span')[0]
        if detail_nodes is not None:
            return detail_nodes.text_content().strip()

    def parse_published_date(self, root):
        detail_nodes = root.xpath('//dd[contains(@itemprop, "publisher")]/preceding-sibling::dd[position()=1]')[0]
        if detail_nodes is not None:
            pub_date_text = detail_nodes.text_content().strip()
            return self._convert_date_text(pub_date_text)

    def _convert_date_text(self, date_text):
        # 8/30/2011
        year = int(date_text[-4:])
        month = 1
        day = 1
        if len(date_text) > 4:
            text_parts = date_text[:len(date_text)-5].partition('/') # m/d
            month = int(text_parts[0])
            if len(text_parts[2]) > 0:
                day = int(text_parts[2])
        from calibre.utils.date import utc_tz
        return datetime.datetime(year, month, day, tzinfo=utc_tz)

    def parse_comments(self, root):
        comments = ''
        
        description_node = root.xpath('//div[contains(@class,"springer-html")]')
        if description_node:
            #self.log('Got a comments description node')
            comments = tostring(description_node[0], method='html').strip()
            # comments = comments.replace('<h2>Overview</h2>','')
            comments = sanitize_comments_html(comments)
            return comments
        
    def parse_cover(self, root):
        img_url = 'http://images.springer.com/sgw/books/big/%s.jpg' % self.parse_isbn(root)

        # self.log.info('Image URL is: %s' % img_url)
        
        # Unfortunately Springer sometimes have broken links so we need to do
        # an additional request to see if the URL actually exists
        info = self.browser.open_novisit(img_url, timeout=self.timeout).info()
        if int(info.getheader('Content-Length')) > 1000:
            return img_url
        else:
            self.log.warning('Broken image for url: %s' % img_url
)
      
    def parse_tags(self, root):
        # Goodreads does not have "tags", but it does have topics
        # We will use those as tags (with a bit of massaging)
        # topics_node = root.xpath('//a[contains(@itemprop, "genre")]')[0]
        topics_node = root.xpath('//div[contains(@class, "breadcrumb channels")]/a')
        #self.log.info("Parsing tags")        
        # self.log.info('%s' % topics_node.text_content())

        tags_to_add = list()
        if topics_node is not None:
            #self.log.info("Found topics")
            # topics = topics_node.text_content().split('/')
            
            for topic in topics_node:
              tags_to_add.append(topic.text_content().strip())

        # self.log.info('Tags list: ')
        # self.log.info('\n'.join(str(p) for p in tags_to_add) )
        return list(tags_to_add)

    def _parse_language(self, root):
        # for now all the languages are english
        # and it can't be avoid since no field indicator the book language
        return 'eng'