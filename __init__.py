#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__ = 'GPL v3'
__copyright__ = '2011, Grant Drake <grant.drake@gmail.com>, 2018 updates by Tri Phan <pttri89@gmail.com>'
__docformat__ = 'restructuredtext en'

import time
from urllib import quote
from Queue import Queue, Empty
from collections import OrderedDict

from lxml.html import fromstring, tostring

# from calibre import as_unicode
# from calibre.ebooks.metadata import check_isbn
# from calibre.ebooks.metadata.sources.base import Source
# from calibre.utils.icu import lower
# from calibre.utils.cleantext import clean_ascii_chars
# from calibre.utils.localization import get_udc

from calibre import as_unicode
from calibre.ebooks import normalize
from calibre.ebooks.metadata import check_isbn
from calibre.ebooks.metadata.sources.base import Source, fixcase, fixauthors
from calibre.utils.icu import lower
from calibre.utils.cleantext import clean_ascii_chars

class Springer(Source):

    name = 'Springer'
    description = _('Downloads metadata and covers from Springer')
    author = 'Tri Phan <pttri89@gmail.com>'
    version = (1, 0, 0)
    minimum_calibre_version = (0, 8, 0)

    capabilities = frozenset(['identify', 'cover'])
    touched_fields = frozenset(['title', 'authors',
        'identifier:isbn', 'rating', 'comments', 'publisher', 'pubdate',
        'series'])
    has_html_comments = True
    supports_gzip_transfer_encoding = True

    BASE_URL = 'http://www.springer.com/gp/book'
    # BROWSE_URL = 'http://www.springer.com/gp/'
    # SEARCH_URL = 'http://www.springer.com/gp/search?'

    def config_widget(self):
        '''
        Overriding the default configuration screen for our own custom configuration
        '''
        from calibre_plugins.springer_metadata.config import ConfigWidget
        return ConfigWidget(self)

    def get_book_url(self, identifiers):
        prints('get_book_url')
        isbn_id = identifiers.get('isbn', None)
        if isbn_id:
            url = '%s/%s' % (Springer.BROWSE_URL, isbn_id)
            return ('springer', isbn_id, url)

    def create_query(self, log, title=None, authors=None, identifiers={}):

        isbn = check_isbn(identifiers.get('isbn', None))
        if isbn is not None:
            # format query to try to avoid "Please enable JavaScript to view the page content." redirect page
            log.info('Search from isbn: %s' %(isbn))
            return '%s/%s' % (Springer.BASE_URL, isbn)

    def get_cached_cover_url(self, identifiers):
        url = None
        springer_isbn_id = identifiers.get('springer_isbn', None)
        if springer_isbn_id is None:
            isbn = identifiers.get('isbn', None)
            if isbn is not None:
                springer_isbn_id = self.cached_isbn_to_identifier(isbn)
        if springer_isbn_id is not None:
            url = self.cached_identifier_to_cover_url(springer_isbn_id)
        return url

    def cached_identifier_to_cover_url(self, id_):
        with self.cache_lock:
            url = self._get_cached_identifier_to_cover_url(id_)
            if not url:
                # Try for a "small" image in the cache
                url = self._get_cached_identifier_to_cover_url('small/' + id_)
            return url

    def _get_cached_identifier_to_cover_url(self, id_):
        # This must only be called once we have the cache lock
        url = self._identifier_to_cover_url_cache.get(id_, None)
        if not url:
            # We could not get a url for this particular B&N id
            # However we might have one for a different isbn for this book
            # Barnes & Noble are not very consistent with their covers and
            # it could be that the particular ISBN we chose does not have
            # a large image but another ISBN we retrieved does.
            for key in self._identifier_to_cover_url_cache.keys():
                if key.startswith('key_prefix'):
                    return self._identifier_to_cover_url_cache[key]
        return url

    def identify(self, log, result_queue, abort, title=None, authors=None,
            identifiers={}, timeout=30):
        '''
        Note this method will retry without identifiers automatically if no
        match is found with identifiers.
        '''
        matches = []
        # If we have a ISBN id then we will go straight to the URL for that book.
        isbn = check_isbn(identifiers.get('isbn', None))
        br = self.browser
        
        query = self.create_query(log, identifiers=identifiers)
        if query is None:
            log.error('Insufficient metadata to construct query')
            return
        multiple_results_found = False
        matches.append(query)
        # try:
        #     log.info('Querying: %s' % query)
        #     response = br.open_novisit(query, timeout=timeout)
        #     # Check whether we got redirected to a book page.
        #     # If we did, will use the url.
        #     # If we didn't then treat it as no matches on Barnes & Noble
        #     location = response.geturl()
        # except Exception as e:
        #     if isbn and callable(getattr(e, 'getcode', None)) and e.getcode() == 404:
        #         # We did a lookup by ISBN but did not find a match
        #         # We will fallback to doing a lookup by title author
        #         log.info('Failed to find match for ISBN: %s' % isbn)
        #     elif callable(getattr(e, 'getcode', None)) and e.getcode() == 404:
        #         log.error('No matches for identify query')
        #         return as_unicode(e)
        #     else:
        #         err = 'Failed to make identify query'
        #         log.exception(err)
        #         return as_unicode(e)

        if abort.is_set():
            return

        from calibre_plugins.springer_metadata.worker import Worker
        workers = [Worker(url, result_queue, br, log, i, self) for i, url in
                enumerate(matches)]

        for w in workers:
            w.start()
            # Don't send all requests at the same time
            time.sleep(0.1)

        while not abort.is_set():
            a_worker_is_alive = False
            for w in workers:
                w.join(0.2)
                if abort.is_set():
                    break
                if w.is_alive():
                    a_worker_is_alive = True
            if not a_worker_is_alive:
                break

        return None

    def _parse_search_results(self, log, orig_title, orig_authors, root, matches, timeout):
        for e in root.iter("span"):
            if "display:none" in e.get("style", "").replace(" ", ""):
                e.text = ""
                
        UNSUPPORTED_FORMATS = ['audiobook', 'other format', 'cd', 'item', 'see all formats & editions']
        result = root.xpath('//div[contains(@class, "product-page")]')

        def ismatch(title, authors):
            authors = lower(' '.join(authors))
            title = lower(title)
            match = not title_tokens
            for t in title_tokens:
                if lower(t) in title:
                    match = True
                    break
            amatch = not author_tokens
            for a in author_tokens:
                if lower(a) in authors:
                    amatch = True
                    break
            if not author_tokens: amatch = True
            return match and amatch

        import calibre_plugins.springer_metadata.config as cfg
        max_results = cfg.plugin_prefs[cfg.STORE_NAME][cfg.KEY_MAX_DOWNLOADS]
        title_url_map = OrderedDict()
        title = result.xpath('.//dd[contains(@itemprop, "name")]')

        title = title[0].text_content().strip()


        contributors = result.xpath('.//li[contains(@itemprop, "editor")]/span')
        authors = []
        for c in contributors:
            author = c.text_content().split(',')[0]
            log.info('Found author:',author)
            if author.strip():
                authors.append(author.strip())

        log.info('Looking at tokens:', author)
        title_tokens = list(self.get_title_tokens(orig_title))
        author_tokens = list(self.get_author_tokens(orig_authors))
        log.info('Considering search result: %s by %s' % (title, ' & '.join(authors)))
        if not ismatch(title, authors):
            log.error('Rejecting as not close enough match: %s by %s' % (title, ' & '.join(authors)))
        


    def download_cover(self, log, result_queue, abort,
         title=None, authors=None, identifiers={}, timeout=30):
        cached_url = self.get_cached_cover_url(identifiers)
        if cached_url is None:
            log.info('No cached cover found, running identify')
            rq = Queue()
            self.identify(log, rq, abort, title=title, authors=authors,
                    identifiers=identifiers)
            if abort.is_set():
                return
            results = []
            while True:
                try:
                    results.append(rq.get_nowait())
                except Empty:
                    break
            results.sort(key=self.identify_results_keygen(
                title=title, authors=authors, identifiers=identifiers))
            for mi in results:
                cached_url = self.get_cached_cover_url(mi.identifiers)
                if cached_url is not None:
                    break
        if cached_url is None:
            log.info('No cover found')
            return

        if abort.is_set():
            return
        br = self.browser
        log('Downloading cover from:', cached_url)
        try:
            cdata = br.open_novisit(cached_url, timeout=timeout).read()
            result_queue.put((self, cdata))
        except:
            log.exception('Failed to download cover from:', cached_url)


if __name__ == '__main__': # tests
    # To run these test use:
    # calibre-debug -e __init__.py
    from calibre import prints
    from calibre.ebooks.metadata.sources.test import (test_identify_plugin,
            title_test, authors_test, series_test)

    def cover_test(cover_url):
        if cover_url is not None:
            cover_url = cover_url.lower()

        def test(mi):
            mc = mi.cover_url
            if mc is not None:
                mc = mc.lower()
            if mc == cover_url:
                return True
            prints('Cover test failed. Expected: \'%s\' found: ' % cover_url, mc)
            return False

        return test

    test_identify_plugin(Springer.name,
        [

            (# A book with an ISBN
                {'identifiers':{'isbn': '9783662560037'},
                    'title':'Janeway Immunologie', 'authors':['Kenneth M. Murphy', 'Casey Weaver']},
                [title_test('Janeway Immunologie', exact=True),
                 authors_test(['Kenneth M. Murphy', 'Casey Weaver']),
                 cover_test('http://images.springer.com/sgw/books/big/9783662560037.jpg')]
            ),

            (# A book with an ISBN
                {'identifiers':{'isbn': '9783319524849'},
                    'title':'Emerging and Re-emerging Viral Infections', 'authors':['Giovanni Rezza', 'Giuseppe Ippolito']},
                [title_test('Emerging and Re-emerging Viral Infections', exact=True),
                 authors_test(['Giovanni Rezza', 'Giuseppe Ippolito']),
                 series_test('Advances in Microbiology, Infectious Diseases and Public Health', 1.0),
                 cover_test('http://images.springer.com/sgw/books/big/9783319524849.jpg')]
            ),

            (# A book with an NA cover
                {'identifiers':{'isbn':'9783319517858'},
                 'title':'Positive Psychology Interventions in Practice', 'authors':['Carmel Proctor']},
                [title_test('Positive Psychology Interventions in Practice', exact=True),
                 authors_test(['Carmel Proctor']),
                 cover_test(None)]
            ),

        ], fail_missing_meta=False)


